    let reqs = {};
    reqs.noproxy = async () => {
      let response = await fetch("${API_URL}")
      return await response.json()
    }
    reqs.proxy = async () => {
      let response = await fetch(window.location.origin + "${PROXY_ENDPOINT}?apiurl=${API_URL}")
      return await response.json()
    }
    reqs.proxypreflight = async () => {
      const reqBody = {
        msg: "Hello world!"
      }
      let response = await fetch(window.location.origin + "${PROXY_ENDPOINT}?apiurl=${API_URL}", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(reqBody),
      })
      return await response.json()
    }
    (async () => {
      for (const [reqName, req] of Object.entries(reqs)) {
        try {
          let data = await req()
          document.getElementById(reqName).innerHTML = JSON.stringify(data)
        } catch (e) {
          document.getElementById(reqName).innerHTML = e
        }
      }
    })();
